from dataload import get_data
from sklearn.ensemble import RandomForestClassifier
from copy import deepcopy
import numpy as np
import sklearn
import os
from PIL import Image

train_share = 0.8

def split_dataset(data: tuple):
	# Splits a dataset into train and test
	x, y = data
	n = len(x)
	n_train = int(n*train_share)
	train = np.array([False]*n)
	train_idcs = np.random.choice(n, n_train, replace=False)
	train[train_idcs] = True
	test = ~train
	
	return x[train], x[test], y[train], y[test]


class Oracle:
	def __init__(self, n: int = None):
		# n is initial number of labeled images
		self.labeled = np.random.choice(60000, n or 60000, replace=False)
		self.unlabeled = np.array([i for i in range(60000) if i not in self.labeled])
		self.shuffle()
		self.xtrain, self.xtest, self.ytrain, self.ytest = get_data()
	
	def set_labels(self, labeled: np.ndarray, unlabeled: np.ndarray):
		self.labeled = labeled.copy()
		self.unlabeled = unlabeled.copy()

	def get_labeled_train(self):
		# Returns all labeled training data
		return self.xtrain[self.labeled], self.ytrain[self.labeled]

	def bagging(self, T: int):
		# Produces T datasets from the labeled training observations as described in ML book
		x, y = [x.copy() for x in self.get_labeled_train()]
		xtrain, xval, ytrain, yval = split_dataset((x, y))

		datasets = [None] * T
		for i in range(T):
			idcs = np.random.choice(len(xtrain), len(xtrain))
			datasets[i] = (xtrain[idcs], xval, ytrain[idcs], yval)
		return datasets
	
	def query(self, n: int, models):
		if len(self.labeled) + n > 60000:
			return None
		# Gets n new labels and updates self.labeled and self.unlabeled
		new_labeled = self._get_new_idcs(n, models)
		self.save_samples(new_labeled)
		self.labeled = np.concatenate((self.labeled, self.unlabeled[new_labeled]))
		self.unlabeled = np.delete(self.unlabeled, new_labeled)
		self.shuffle()
		return new_labeled
	
	def save_samples(self, labels):
		p = f"local_data/queries/{str(self).replace(' ', '_')}"
		files = os.listdir(p)
		nlabs = int(np.sqrt(min(len(labels), 100)))
		if not nlabs: return
		labels = labels[:nlabs**2]
		imgarr = np.empty((28*nlabs, 28*nlabs), dtype=np.uint8)
		for i in range(nlabs):
			for j in range(nlabs):
				u = self.unlabeled[i*nlabs+j]
				imgpart = imgarr[28*j:28*(j+1), 28*i:28*(i+1)]
				imgpart[...] = self.xtrain[u].astype(np.uint8).reshape((28, 28))
		img = Image.fromarray(imgarr)
		img.save(f"{p}/query_{len(files)+1}.png")
	
	def shuffle(self):
		np.random.shuffle(self.labeled)
		np.random.shuffle(self.unlabeled)
	
	def _get_new_idcs(self, n: int, models):
		raise NotImplementedError

	def __str__(self):
		raise NotImplementedError

class RandomOracle(Oracle):
	def _get_new_idcs(self, n: int, models):
		new_labeled = np.random.choice(len(self.unlabeled), n, replace=False)
		return new_labeled
	def __str__(self):
		return "Random Oracle"

class LeastConfident(Oracle):
	# testacc_qbc=[]
	# ncomms = 10
	# poolidx=np.arange(len(self.xtrain),dtype=np.int)
	# poolidx=np.setdiff1d(poolidx,x[train])
	def _get_new_idcs(self, n: int, models):
		if not models:
			return []
		x = self.xtrain[self.unlabeled]
		all_idcs = np.arange(len(x))
		votes = np.zeros((len(x), 10))
		for model in models:
			preds = model.predict(x)
			votes[all_idcs, preds] += 1
		largest = votes.argmax(axis=1)
		confidence = len(models) - largest
		new_labeled = confidence.argsort()[-n:]
		return new_labeled
	
	def __str__(self):
		return "Least Confident"

class MarginSampling(Oracle):
	def _get_new_idcs(self, n: int, models):
		if not models:
			return []
		x = self.xtrain[self.unlabeled]
		all_idcs = np.arange(len(x))
		votes = np.zeros((len(x), 10))
		for model in models:
			preds = model.predict(x)
			votes[all_idcs, preds] += 1
		largest = votes.argmax(axis=1)
		l_votes = votes[all_idcs, largest].copy()
		votes[all_idcs, largest] = -float("inf")
		second_largest = votes.argmax(axis=1)
		votes[all_idcs, largest] = l_votes
		margins = votes[all_idcs, largest] - votes[all_idcs, second_largest]
		new_labeled = margins.argsort()[:n]

		return new_labeled
	
	def __str__(self):
		return "Margin Sampling"

class Entropy(Oracle):
	def _get_new_idcs(self, n: int, models):
		if not models:
			return []
		x = self.xtrain[self.unlabeled]
		all_idcs = np.arange(len(x))
		votes = np.zeros((len(x), 10))
		for model in models:
			preds = model.predict(x)
			votes[all_idcs, preds] += 1
		# assert votes.sum() == len(models) * len(self.unlabeled)
		probs = votes / len(models)
		# assert np.isclose(probs.sum(axis=1), 1).all()
		classwise_entropy = probs * np.log(probs)
		classwise_entropy[votes==0] = 0
		entropy = -np.sum(classwise_entropy, axis=1)
		# assert not np.isnan(entropy).any()
		# Get n largest entropies in linear time using magic
		# https://stackoverflow.com/questions/6910641/how-do-i-get-indices-of-n-maximum-values-in-a-numpy-array
		new_labeled = np.argpartition(entropy, -n)[-n:]
		# new_labeled = entropy.argsort()[-n:]
		return new_labeled
	
	def __str__(self):
		return "Entropy"


class ExpectedImpact(Oracle):
	def _get_new_idcs(self, n: int, models):
		if not models:
			return []
		# TODO

		return new_labeled
	def __str__(self):
		return "Expected Impact"


