# Importing the required packages 
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.exceptions import UndefinedMetricWarning
import warnings
warnings.filterwarnings("ignore", category=UndefinedMetricWarning)

from oracles import split_dataset, Oracle, RandomOracle

# Function to perform training with giniIndex. 
def train_dt(xtrain, ytrain, criterion, max_depth, min_samples_leaf, max_features="sqrt", random_state=None):
	clf = DecisionTreeClassifier(
		criterion=criterion,
		max_depth=max_depth,
		min_samples_leaf=min_samples_leaf,
		max_features="sqrt",
		random_state=random_state,
	)

	# Performing training 
	clf.fit(xtrain, ytrain) 
	return clf


# Function to make predictions 
def prediction(X, clf_object):
	# Predicton on test with giniIndex 
	y_pred = clf_object.predict(X)
	return y_pred

def vote_pred(x, classifiers):
	# Predicts classes using the votes of multiple classifiers
	all_idcs = np.arange(len(x))
	votes = np.zeros((len(x), 10), dtype=int)
	for clf in classifiers:
		pred = prediction(x, clf)
		votes[all_idcs, pred] += 1
	return np.argmax(votes, axis=1)

# Function to calculate accuracy 
def accuracy(y_test, y_pred):

	return confusion_matrix(y_test, y_pred),\
		   accuracy_score(y_test, y_pred),\
		   classification_report(y_test, y_pred)

# Driver code 
def forest(oracle: Oracle,
				   t=100,
				   criterion="gini",
				   max_depth=10,
				   min_samples_leaf=3,
				   max_features="sqrt",
				   random_state=None):
	# Trains n decision trees using bagging and evaluates all
	# If max_features == "sqrt", it is a random forest [ML book, p. 284]
	# oracle should be subclass of oracle

	datasets = oracle.bagging(t)
	classifiers = [None] * t
	args = []
	for i, (xtrain, xval, ytrain, yval) in enumerate(datasets):
		classifiers[i] = train_dt(xtrain, ytrain, criterion, max_depth, min_samples_leaf, max_features, random_state)

	pred = vote_pred(xval, classifiers)
	cm, acc, report = accuracy(yval, pred)
	
	return classifiers, cm, acc, report


if __name__=="__main__": 
	oracle = RandomOracle(1000)
	classifiers, cm, acc, report = forest(oracle)
	pred = vote_pred(oracle.xtest, classifiers)
	cm, acc, report = accuracy(oracle.ytest, pred)
	print(cm)
	print(acc)
	print(report)

