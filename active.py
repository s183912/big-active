import os, sys
os.chdir(sys.path[0])
import matplotlib.pyplot as plt
import numpy as np
from decisiontree import forest, vote_pred, accuracy
from oracles import Oracle, RandomOracle, LeastConfident, MarginSampling, Entropy
from time import time
from typing import List
from logger import Logger
from shutil import rmtree
log = Logger("local_data/active.log", "Training of different oracles")

def active(oracle: Oracle, queries, random_state=None):
	log.section(f"Training oracle {oracle}")
	val_data_points = []
	val_accs = []
	test_data_points = []
	test_accs = []
	query_labels = np.diff(np.logspace(np.log10(len(oracle.labeled)), np.log10(60000), queries)).astype(int)
	query_labels = [0, *query_labels]
	classifiers = []
	for i, q_labs in enumerate(query_labels):
		assert oracle.query(q_labs, classifiers) is not None
		val_data_points.append(len(oracle.labeled))
		start = time()
		classifiers, cm, acc, report = forest(oracle, random_state=random_state)
		dt = time() - start
		val_accs.append(acc)
		test_data_points.append(len(oracle.labeled))
		test_pred = vote_pred(oracle.xtest, classifiers)
		cm, acc, report = accuracy(oracle.ytest, test_pred)
		test_accs.append(acc)
		log("\n".join([
			f"Iteration:       {i+1} / {len(query_labels)}",
			f"Data points:     {val_data_points[-1]}",
			f"New data points: {q_labs}",
			f"Validation acc.: {val_accs[-1]*100:.2f} %",
			f"Test acc.:       {test_accs[-1]*100:.2f} %",
			f"Training time:   {dt:.2f} s",
		]))
	del oracle
	return tuple(np.array(x) for x in (val_data_points, val_accs, test_data_points, test_accs))

def plot(data_points: np.ndarray, accs: np.ndarray, label: str, title: str):
	plt.plot(data_points, accs*100, label=label)
	plt.title(title)
	plt.semilogx()
	plt.xticks(np.logspace(2, 4, 3))
	plt.xlabel("Data points (80 % training and 20 % validation)")
	plt.ylabel(r"Accuracy [%]")
	plt.grid(True)

def many_actives(oracles: List[Oracle], initial_labels: int, queries: int, n: int, random_state=None):
	val_dps = list()
	val_accs = list()
	test_dps = list()
	test_accs = list()
	labels = list()
	for oracle in oracles:
		va = np.zeros(queries)
		ta = np.zeros(queries)
		for i in range(n):
			o = oracle(initial_labels)
			os.makedirs(f"{qpath}/{str(o).replace(' ', '_')}", exist_ok=True)
			val_data_points, val_accs_, test_data_points, test_accs_ = active(o, queries, random_state)
			va += val_accs_
			ta += test_accs_
		val_dps.append(val_data_points)
		test_dps.append(test_data_points)
		labels.append(str(o))
		val_accs.append(va/n)
		test_accs.append(ta/n)
	plt.figure(figsize=(15, 10))
	for dps, accs, label in zip(val_dps, val_accs, labels):
		plot(dps, accs, label, f"Avg. validation accuracy for {n} runs")
	plt.legend(loc=2)
	plt.savefig("local_data/validation.png")
	plt.clf()
	log("Saved validation accuracy plot to local_data/validation.png")
	for dps, accs, label in zip(test_dps, test_accs, labels):
		plot(dps, accs, label, f"Avg. test accuracy for {n} runs")
	plt.legend(loc=2)
	plt.savefig("local_data/test.png")
	plt.clf()
	log("Saved test accuracy plot to local_data/test.png")

if __name__ == "__main__":
	# Cleans up directory
	os.makedirs("local_data/queries", exist_ok=True)
	qpath = "local_data/queries"
	for d in os.listdir(qpath):
		rmtree(f"{qpath}/{d}")
	# np.random.seed(100)
	initial_labels = 100
	queries = 40
	oracles = [
		RandomOracle,
		LeastConfident,
		MarginSampling,
		Entropy,
	]
	log(f"Testing oracles\n{', '.join(str(x) for x in oracles)}\nInitially known labels: {initial_labels}\nQueries: {queries}")
	# Ensure same starting data points for comparability
	many_actives(oracles, initial_labels, queries, 10)


