import os
import torch
import torchvision
import numpy as np

def get_data(dataset="MNIST"):
	os.makedirs("local_data", exist_ok=True)
	datagetter = getattr(torchvision.datasets, dataset)
	datagetter("local_data", download=True)
	xtrain, ytrain = torch.load(f"local_data/{dataset}/processed/training.pt")
	xtest, ytest = torch.load(f"local_data/{dataset}/processed/test.pt")
	return xtrain.numpy().reshape((-1, 784)), xtest.numpy().reshape((-1, 784)), ytrain.numpy(), ytest.numpy()



if __name__ == "__main__":
	xtrain, xtest, ytrain, ytest = get_data()
	print(xtrain.shape, xtest.shape)
	print(ytrain.shape, ytest.shape)
